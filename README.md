# SSI based authorization for Cross - border government and business representatives in logistics - CorpoSign DID (aka.eSSIF4Logistics)
===================

# Introduction
One of the biggest challenges of smooth cross-border e-CMR process adoption (a CMR is a waybill which is often inspected by controlling agencies such as the police, customs agents, etc.) is conditioned by the lack of harmonized approach and technical solution enabling to authorize representatives electronically. There is a consensus of the logistic ecosystem community (representing >0.5 million transport organizations are registered in the EU and government authorities acting in the field) on the need for a interoperable and scalable solution for authorization of natural persons representing a legal person (government and business organizations) in the cross-border e-CMR process as current authorization methods (eID, eSignature) have limitations – it is not possible to name a person's relationship with the organization, as well as it is difficult to grant access to act in the company’s name to employee if he works in a different country than the company is based on, etc.   

# Summary
SIS is developing an SSI solution in the e-Government area that will enable government and business organizations operating in the cross-border logistics ecosystem to authorize their representatives in order to perform appropriate actions on behalf of the organization using an ecosystem-specific scheme (with required attributes). Currently there are no authorization mechanisms that could link natural persons to organization in the cross-border logistics ecosystem. Problems occur if the organization and the representative are from different countries or the representative does not agree to use their personal identifier for organizational purposes. As well, lack of such a solution results in inability to ensure interoperability, security, and to prevent fraud. ESSIF4Log will allow to authorize the representative (by issuing him verifiable credentials). It will ensure better governance, security, interoperability, will become an enabler to speed up the eCMR development/implementation process. 

![ESSIF4Logistics concept!](https://gitlab.grnet.gr/essif-lab/cfoc/sisuab/deliverables/-/raw/master/Images/eSSIF4Logistics-concept_v2.png)

CorpoSign DID is a blockchain-agnostic, decentralized digital identification platform. It is comprised of three separate products, including CorpoSign DID Issuer, Wallet, and Verify.

# Solution functionalities
1. CorpoSign DID | Issuer - Credentials issuance and validation platform for trusted parties and official authorities to create and certify digital identity credentials according to W3C and EBSI V2 standards.
2. CorpoSign DID | Wallet - Mobile Digital ID wallet for users to store identity credentials from any issuer worldwide and seamlessly authenticate in digital services.
3. CorpoSign DID | Verify - Single-sign-on & authentication tools for businesses that automatically verify credentials and documents to manage consent proofs.


# Useful links
Developed by <a href="https://www.sis.lt" target="_blank">**Systems Integration Solutions, UAB**</a> <br/>
Project  <a href="https://essif-lab.pages.grnet.gr/framework/docs/essifLab-project" target="_blank">**The eSSIF-Lab Project**</a><br/>
Demo instructions:  <a href="https://sis.lt/mydid-manual-for-users/" target="_blank">**My-did Instructions (v1 CorpoSign DID)**</a> <br/>
